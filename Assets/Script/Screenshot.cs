﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class Screenshot : MonoBehaviour 
{
	public GameObject save_but;
	public GameObject hint_txt;
	public GameObject restart;
	public GameObject gameclose;
	public GameObject scorepanel;

    public RawImage pic;
    public Texture2D textu;
    public Color32[] imageColor = new Color32[10];

    public int totalR;
    public int totalG;
    public int totalB;
    public bool red;
    public bool green;
    public bool blue;

    void Update ()
	{
        if (Input.GetKeyDown(KeyCode.A))
        {
            totalR = 0;
            totalG = 0;
            totalB = 0;
            StartCoroutine(TakeShot_1());
        }
        if (totalR > totalG && totalR > totalB)
        {
            print("紅");
        }
        if (totalG > totalR && totalG > totalB)
        {
            print("綠");
        }
        if (totalB > totalR && totalB > totalG)
        {
            print("藍");
        }
    }
	public void Shot ()
	{
		hint_txt.SetActive (false);
		save_but.SetActive (false);
		restart.SetActive (false);
		gameclose.SetActive (false);
		scorepanel.transform.localScale = new Vector3 (0.9f,0.9f,0);

		StartCoroutine (TakeShot());
	}
	private IEnumerator TakeShot()
	{
		yield return new WaitForEndOfFrame();
		System.DateTime now = System.DateTime.Now; 
		string times = now.ToString (); 
		times = times.Trim (); 
		times = times.Replace ("/","-"); 
		//文件名
		string filename = "Screenshot"+times+".png"; 

		Texture2D texture = new Texture2D (Screen.width, Screen.height, TextureFormat.RGB24, false); 
		texture.ReadPixels (new Rect (0, 0, Screen.width, Screen.height), 0, 0); 
		texture.Apply (); 
		//转为字节数组 
		byte[] bytes = texture.EncodeToPNG ();
		string destination = "/sdcard/DCIM/MagicForest"; 

		//判断目录是否存在，不存在则会创建目录 
		if (!Directory.Exists (destination)) 
		{ 
			Directory.CreateDirectory (destination); 
		} 
		//文件路径
		string Path_save = destination + "/" + filename;
		//string Path_save = Application.persistentDataPath;
		//存图片 
		//ScreenCapture.CaptureScreenshot(Path_save);
		System.IO.File.WriteAllBytes (Path_save, bytes);

		yield return new WaitForSeconds(1);
		save_but.SetActive (true);
		hint_txt.SetActive (true);
		restart.SetActive (true);
		gameclose.SetActive (true);
		scorepanel.transform.localScale = new Vector3 (0.7f,0.7f,0);
		yield return new WaitForSeconds(10);
		hint_txt.SetActive (false);
	}

    private IEnumerator TakeShot_1()
    {
        yield return new WaitForEndOfFrame();

        textu = new Texture2D(Screen.width, Screen.height, TextureFormat.RGB24, false);
        textu.ReadPixels(new Rect(0, 0, Screen.width, Screen.height), 0, 0);
        textu.Apply();

        imageColor[0] = textu.GetPixel(540, 960);
        imageColor[1] = textu.GetPixel(520, 940);
        imageColor[2] = textu.GetPixel(510, 920);
        imageColor[3] = textu.GetPixel(500, 900);
        imageColor[4] = textu.GetPixel(480, 880);
        imageColor[5] = textu.GetPixel(560, 980);
        imageColor[6] = textu.GetPixel(580, 1000);
        imageColor[7] = textu.GetPixel(600, 1020);
        imageColor[8] = textu.GetPixel(620, 1040);
        imageColor[9] = textu.GetPixel(640, 1060);

        //pic.transform.position = textu.GetPixel(0, 0);
        pic.texture = textu;

        for (int r = 0; r < 10; r++)
        {
            totalR = totalR + imageColor[r].r;
            totalG = totalG + imageColor[r].g;
            totalB = totalB + imageColor[r].b;
        }
    }
}
