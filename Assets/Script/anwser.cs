﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class anwser : MonoBehaviour
{
	public bool anwsered;
	public int score;
	public string right_ans;
    public int check = 0;

    public Animator[] anis;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        anis = GetComponentsInChildren<Animator>();
        if (anwsered)
        {
            check_fn(anis[0]);
            check_fn(anis[1]);
            check_fn(anis[2]);
            check_fn(anis[3]);
        }
    }
	private void check_fn (Animator xx) 
	{
        if (xx.GetComponent<rotate>().check)
        {
            xx.SetBool("choose", true);
        }
        else
        {
            xx.SetBool("done", true);
        }
    }
}
